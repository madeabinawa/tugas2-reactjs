import React from 'react';
import logo from './logo.svg';
import './App.css';
import Field from './components/Field/Field'
import Button from './components/Button/Button'
import Title from './components/Title/Title'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Title title="Selamat Datang di Aplikasi React"/>
        <Field pholder={"Username"}/>
        <Field pholder={"Password"} />
        <Button btnName={"Login"}/> 
      </header>
    </div>
  );
}

export default App;
